# EmacsConf

## A promotional poster / flyer for the EmacsConf 2020 conference.

- `emacsconflogo1-256.png`: The EmacsConf 2020 logo
- `emacsGUI.png`: GNU Emacs GUI screenshot (taken by Paul Sutton)
- `emacsTerm.png`: GNU Emacs terminal screenshot (taken by Paul Sutton)
- `emacsconf.odg`: the original source file
- `emacsconf.pdf`: PDF version
- `emacsconf.png`: PNG version
- `LICENSE`: a copy of the CC BY-SA 4.0 license
- `README.md`: this file

I created this to help promote the conference.  Please feel free to
change and modifiy to suit and share accordingly.

The materials in this repository are licensed under the terms of the
Creative Commons Attribution-ShareAlike 4.0 International Public
License Creative Commons Attribution-ShareAlike 4.0 International
Public License, a copy of which is available in the `LICENSE` file.
